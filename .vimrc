set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
" Automatic code completion tabnine-vim
"Syntax checker for code
Plugin 'vim-syntastic/syntastic'
"Plugin 'zxqfl/tabnine-vim'
"Showing powerline in vim
"Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
"Shows indentation with vertical line
Plugin 'yggdroot/indentline'
"Sensible defaults for vim
Plugin 'tpope/vim-sensible'
"Format code
Plugin 'google/yapf', { 'rtp': 'plugins/vim' }
"Follow PEP8 indentation
"Plugin 'vim-scripts/indentpython.vim'
"For python
Plugin 'nvie/vim-flake8'
"Awesome utility to paste code
Plugin 'roxma/vim-paste-easy'
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
augroup vimrc_autocmds
autocmd!
" highlight characters past column 120
autocmd FileType python highlight Excess ctermbg=DarkGrey guibg=Black
autocmd FileType python match Excess /\%120v.*/
autocmd FileType python set nowrap
augroup END
" syntax highlighting
syntax on
set number
set cursorline
set showmatch
let python_highlight_all = 1
set nobackup
set nowb
set noswapfile
" Tesla Coding Standard: tab business
set noet
set ts=4
set sw=4
set smartindent
set expandtab
let g:vim_json_conceal=0
filetype plugin indent on    " required
let python_highlight_all=1
syntax on
au BufNewFile,BufRead *.py set tabstop=4
au BufNewFile,BufRead *.py set softtabstop=4
au BufNewFile,BufRead *.py set shiftwidth=4
au BufNewFile,BufRead *.py set textwidth=79
au BufNewFile,BufRead *.py set expandtab
au BufNewFile,BufRead *.py set autoindent
au BufNewFile,BufRead *.py set fileformat=unix
set nu
set guifont=DejaVu\ Sans\ Mono\ for\ Powerline\ 9
set laststatus=2 
