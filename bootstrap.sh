sudo apt update
sudo apt install -y curl git openssh-server openssh-client vim byobu
sudo systemctl enable ssh.service
sudo service ssh restart
git clone https://gitlab.com/saradhix/init.git
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
cp -v $HOME/init/.vimrc $HOME
echo "Please ssh to the server and continue installing other software"
git config --global user.name Saradhi
git config --global user.email saradhix@yahoo.com
sudo apt install -y zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
