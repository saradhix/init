curl -o ~/miniconda.sh https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x ~/miniconda.sh
~/miniconda.sh -b -p ~/conda
~/conda/bin/conda install -y python=3.10
echo ". $HOME/conda/etc/profile.d/conda.sh" >> ~/.zshrc
