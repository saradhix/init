curl -o ~/miniconda.sh -O https://repo.anaconda.com/archive/Anaconda3-2021.04-Linux-aarch64.sh
chmod +x ~/miniconda.sh
rm -rf ~/conda
~/miniconda.sh -b -p ~/conda
~/conda/bin/conda install -y python=3.8
echo ". $HOME/conda/etc/profile.d/conda.sh" >> ~/.zshrc
