echo "Installing ohmyzosh plugins"

curl -fLOs https://script.install.devinsideyou.com/zsh-autocomplete
chmod +x zsh-autocomplete && ./zsh-autocomplete

curl -fLOs https://script.install.devinsideyou.com/zsh-z
chmod +x zsh-z && ./zsh-z

curl -fLOs https://script.install.devinsideyou.com/zsh-syntax-highlighting
chmod +x zsh-syntax-highlighting && ./zsh-syntax-highlighting

curl -fLOs https://script.install.devinsideyou.com/zsh-autosuggestions
chmod +x zsh-autosuggestions && ./zsh-autosuggestions

#Install powerlevel10k with ohmyzsh
echo "Installing powerlevel10k plugin"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
cp -v zshrc ~/.zshrc
cp -v p10.zsh ~/.p10k.zsh


#echo "Installing pyenv dependencies"

#sudo apt install -y curl git-core gcc make zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev libssl-dev libffi-dev
#curl https://pyenv.run | bash

#source ~/.zshrc

#pyenv install 3.8.0
#pyenv global 3.8.0
